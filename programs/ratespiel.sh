rand=$(((RANDOM % 10)))
echo -e "Ich habe eine Zahl zwischen 0 und 9 gewählt.:" 
echo -e "Welche ist es?"
read ui
while (($rand != $ui)); do
	echo -e "Leider falsch :-/\nProbiere es nochmal!"
	read ui
done;
echo -e "RICHTIG! Meine Zahl war die $rand :-)"
