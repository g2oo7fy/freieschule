echo "Klein Häschen geht in die Bäckerei und fragt den Bäcker: 'Hadu Mööan???'"
echo "Der Bäcker sagt nein und das Häschen geht wieder nach Hause."

cntnu=true

while [ $cntnu = true ]; do
	n=0
	while [ $n -lt 2 ]; do
	##		echo "n hat den Wert "$n
		echo "Am nächsten Tag kommt das Häschen wieder und fragt:"
		echo "'Hadu Mööan???'"
		echo "Der Bäcker sagt wieder nein und das Häschen geht."
		echo "Soll das Häschen nochmal zum Bäcker kommen und fragen?"
		echo -n "J/N:"
		str0="_"
			while [ $str0 != "J" ] && [ $str0 != "j" ]; do
			read str0
			if [ $str0 = "n" ] || [ $str0 = "N" ]; then
				cntnu=false;
				echo "Spielverderber :-("
				n=2
				break
			fi
		done;
		n=$(($n+1))
	done;

	echo "Am dritten Tag hat der Bäcker genug von der Fragerei."
	echo "Was soll er machen? Wähle aus:"
	echo "1. Er besorgt eine Möhre."
	echo "2. Er ranzt das Häschen an."

	read str1

	case $str1 in 
		1) 
			echo "Am nächsten Tag kommt das Häschen wieder und fragt: 'Hadu Mööan?'"
			echo "Stolz sagt der Bäcker: 'Ja, ich habe eine besort!'"
			echo "Entgegnet das Häschen: 'Muddu essen! Is gesund.'"
			;;
		2) 
			echo "Der Bäcker schnauzt das Häschen an:"
			echo "'Wenn Du noch einmal hierher kommst und nach Möhren Fragst,"
			echo "dann nagele ich Dich neben den Heiligen Jesus an die Wand!"
			echo "Wähle zwischen dem frechen Häschen und dem braven!"
			echo "1. Frech"
			echo "2. Brav"
			read str2
			case $str2 in
				1)
					echo "Das Häschen fragt: 'Haddu Nägel?'"
					echo "Sagt der Bächer 'Nein'. Darauf das Häschen:"
					echo "'Haddu Mööan?'"
					;;
				2)
					echo "Das Häschen geht nach Hause, kommt aber am kommenden Tag wieder:"
					echo "'Haddu Mööan?'"
					echo "Der Bäcker wird wütend, packt das Häschen an den Ohren"
					echo "und nagelt es neben das kunstvoll geschnitzte Kreuz."
					echo "Baumelnd blickt sich das Häschen um und erblickt"
					echo "die Jesusfigur auf dem Kreuz:"
					echo "'Haddu auch nach Mööan gefragt?'"
					;; 
			esac
			;;
	esac
done;
