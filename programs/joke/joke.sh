echo ""
echo -e "\t\e[34mKlein Häschen geht in die Bäckerei und fragt den Bäcker: \e[31m'Hadu Mööan???'"
echo -e "\t\e[34mDer Bäcker sagt nein und das Häschen geht wieder nach Hause."

cntnu=true

while [ $cntnu = true ]; do
	n=0
	while [ $n -lt 2 ]; do
	##		echo "n hat den Wert "$n
		echo ""
		echo -e "\t\e[34mAm nächsten Tag kommt das Häschen wieder und fragt:"
		echo -e "\t\e[31m'Hadu Mööan???'"
		echo -e "\t\e[34mDer Bäcker sagt wieder nein und das Häschen geht."
		echo ""
		echo -e "\e[0mSoll das Häschen nochmal zum Bäcker kommen und fragen?"
		echo ""
		echo -n "Gib' J für ja und N für nein ein: "
		read str0
		case $str0 in
			j|J) 
				n=$(($n+1))
				;;
			n|N)
#				cntnu=false;
				echo ""
				echo "Spielverderber :-("
				n=2
				break
				;;
		esac
				

#		str0="_"
#			while [ $str0 != "J" ] && [ $str0 != "j" ]; do
#			read str0
#			if [ $str0 = "n" ] || [ $str0 = "N" ]; then
#				cntnu=false;
#				echo "Spielverderber :-("
#				n=2
#				break
#			fi
#		done;
#		n=$(($n+1))
	done;

	echo ""
	echo -e "\t\e[34mAm dritten Tag hat der Bäcker genug von der Fragerei."
	echo ""
	echo -e "\e[0mWas soll er machen? Wähle aus (gib' 1 oder 2 ein):"
	echo "1. Er besorgt eine Möhre."
	echo "2. Er ranzt das Häschen an."

	read str1

	case $str1 in 
		1) 
			echo ""
			echo -e "\t\e[34mAm nächsten Tag kommt das Häschen wieder und fragt: \e[31m'Hadu Mööan?'"
			echo -e "\tStolz sagt der Bäcker: \e[32m'Ja, ich habe eine besort!'"
			echo -e "\t\e[34mEntgegnet das Häschen: \e[5m\e[31m'Muddu essen! Is gesund.'\e[0m"
			n=0
			cntnu=true
			;;
		2) 
			echo -e "\t\e[34mDer Bäcker schnauzt das Häschen an:"
			echo -e "\t\e[32m'Wenn Du noch einmal hierher kommst und nach Möhren fragst,"
			echo -e "\tdann nagele ich Dich neben den Heiligen Jesus an die Wand!'"
			echo ""
			echo -e "\e[0mWähle zwischen dem frechen Häschen und dem braven!"
			echo -e "1. Frech"
			echo -e "2. Brav"
			read str2
			case $str2 in
				1)
					echo -e "\t\e[34mDas Häschen fragt: 'Haddu Nägel?'"
					echo -e "\tSagt der Bächer \e[32m'Nein'\e[34m. Darauf das Häschen:"
					echo -e "\t\e[5m\e[31m'Haddu Mööan?'\e[0m"
					n=0
					cntnu=true
					;;
				2)
					echo -e "\t\e[34mDas Häschen geht nach Hause, kommt aber am kommenden Tag wieder:"
					echo -e "\t\e[31m'Haddu Mööan?'"
					echo -e "\t\e[34mDer Bäcker wird wütend, packt das Häschen an den Ohren"
					echo -e "\tund nagelt es neben das kunstvoll geschnitzte Kreuz."
					echo -e "\tBaumelnd blickt sich das Häschen um und erblickt"
					echo -e "\tdie Jesusfigur neben sich:"
					echo -e "\t\e[5m\e[31m'Haddu auch nach Mööan gefragt?'"
					cntnu=false
					;; 
			esac
			;;
	esac
done;
